// Copyright (c) 2016 Tais P. Hansen. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

library google_oauth2_jwt.jwt.token;

import 'package:crypto/crypto.dart' show sha256;
import 'package:cryptoutils/cryptoutils.dart' show CryptoUtils;
import 'package:dart_jwt/src/jwt.dart'
    show JsonWebToken, JwtClaimSetValidationContext;
import 'package:dart_jwt/src/jwt_claimset.dart' show OpenIdJwtClaimSet;
import 'package:dart_jwt/src/validation_constraint.dart'
    show ConstraintViolation;

/**
 * Decodes JWT received from Google OAuth2 authentication.
 */
JsonWebToken<GoogleOpenIdClaimSet> decodeGoogleIdToken(String jwt) {
  return new JsonWebToken.decode(jwt,
      claimSetParser: (Map json) => new GoogleOpenIdClaimSet.fromJson(json));
}

/**
 * A [JwtClaimSet] for use with Google OAuth2 id tokens.
 */
class GoogleOpenIdClaimSet extends OpenIdJwtClaimSet {
  // OpenID Connect claim names.
  final String atHash; // Access Token hash value
  final String
      azp; // Authorized party - the party to which the ID Token was issued
  final String cHash; // Code hash value
  final String email; // Preferred e-mail address
  final bool
      emailVerified; // True if the e-mail address has been verified; otherwise false

  // Google-specific claim names.
  final String hd; // Hosted domain

  GoogleOpenIdClaimSet(String issuer, String subject, DateTime expiry,
      DateTime issuedAt, List<String> audience,
      {String this.atHash,
      String this.azp,
      String this.cHash,
      String this.email,
      bool this.emailVerified,
      String this.hd})
      : super(issuer, subject, expiry, issuedAt, audience);

  GoogleOpenIdClaimSet.fromJson(Map json)
      : super.fromJson(json),
        atHash = json['at_hash'],
        azp = json['azp'],
        cHash = json['c_hash'],
        email = json['email'],
        emailVerified = json['email_verified'],
        hd = json['hd'];

  @override
  Map toJson() => super.toJson()
    ..remove('aud')
    ..addAll({
      'aud': (audience.length == 1 ? audience.first : audience),
      'at_hash': atHash,
      'azp': azp,
      'c_hash': cHash,
      'email': email,
      'email_verified': emailVerified,
      'hd': hd
    });

  @override
  GoogleOpenIdClaimSet copy(
      {String issuer,
      String subject,
      DateTime expiry,
      DateTime issuedAt,
      List<String> audience,
      String atHash,
      String azp,
      String cHash,
      String email,
      bool emailVerified,
      String hd}) {
    return new GoogleOpenIdClaimSet(
        issuer != null ? issuer : this.issuer,
        subject != null ? subject : this.subject,
        expiry != null ? expiry : this.expiry,
        issuedAt != null ? issuedAt : this.issuedAt,
        audience != null ? audience : this.audience,
        atHash: atHash != null ? atHash : this.atHash,
        azp: azp != null ? azp : this.azp,
        cHash: cHash != null ? cHash : this.cHash,
        email: email != null ? email : this.email,
        emailVerified:
            emailVerified != null ? emailVerified : this.emailVerified,
        hd: hd != null ? hd : this.hd);
  }

  @override
  Set<ConstraintViolation> validate(
      GoogleClaimSetValidationContext validationContext) {
    Set<ConstraintViolation> violations = super.validate(validationContext);

    if (validationContext._atHash != null &&
        validationContext._atHash != atHash) {
      violations
          .add(new ConstraintViolation('Access token hash does not match: '
              'Expected "${validationContext._atHash}", got "${atHash}"'));
    }

    if (validationContext._cHash != null && validationContext._cHash != cHash) {
      violations.add(new ConstraintViolation('Code hash does not match: '
          'Expected "${validationContext._cHash}", got "${cHash}"'));
    }

    return violations;
  }
}

/**
 * A [JwtClaimSetValidationContext] for validating GoogleClaimSets.
 *
 * It is currently assumed to be SHA256 encoded (HS256, RS256) when validating
 * accessToken and code using at_hash and c_hash.
 */
class GoogleClaimSetValidationContext extends JwtClaimSetValidationContext {
  final String _atHash;
  final String _cHash;

  GoogleClaimSetValidationContext({String accessToken, String code})
      : _atHash = _base64UrlNoPad(accessToken),
        _cHash = _base64UrlNoPad(code);
}

String _base64UrlNoPad(String s) {
  return s == null
      ? s
      : CryptoUtils
          .bytesToBase64(
              (sha256.newInstance()).convert(s.codeUnits).bytes.sublist(0, 16),
              true)
          .split('=')[0];
}
