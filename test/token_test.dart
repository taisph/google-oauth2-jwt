// Copyright (c) 2016 Tais P. Hansen. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

library google_oauth2_jwt.jwt.token_test;

import 'package:dart_jwt/dart_jwt.dart';
import 'package:google_oauth2_jwt/google_oauth2_jwt.dart';
import 'package:test/test.dart';

import 'test_utils.dart';

main() {
  group('google-claim-set', () {
    test('construct', () {
      expect(
          () => new GoogleOpenIdClaimSet(
              TestGoogleIdToken['iss'],
              TestGoogleIdToken['sub'],
              new DateTime.fromMillisecondsSinceEpoch(TestGoogleIdToken['exp']),
              new DateTime.fromMillisecondsSinceEpoch(TestGoogleIdToken['iat']),
              [TestGoogleIdToken['aud']]),
          returnsNormally);

      expect(
          () => new GoogleOpenIdClaimSet(
              TestGoogleIdToken['iss'],
              TestGoogleIdToken['sub'],
              new DateTime.fromMillisecondsSinceEpoch(TestGoogleIdToken['exp']),
              new DateTime.fromMillisecondsSinceEpoch(TestGoogleIdToken['iat']),
              [TestGoogleIdToken['aud']],
              atHash: TestGoogleIdToken['at_hash'],
              azp: TestGoogleIdToken['azp'],
              cHash: TestGoogleIdToken['c_hash'],
              email: TestGoogleIdToken['email'],
              emailVerified: TestGoogleIdToken['email_verified'],
              hd: TestGoogleIdToken['hd']),
          returnsNormally);

      expect(() => new GoogleOpenIdClaimSet.fromJson(TestGoogleIdToken),
          returnsNormally);

      final cs = new GoogleOpenIdClaimSet.fromJson(TestGoogleIdToken);
      expect(cs.atHash, equals(TestGoogleIdToken['at_hash']));
      expect(cs.azp, equals(TestGoogleIdToken['azp']));
      expect(cs.cHash, equals(TestGoogleIdToken['c_hash']));
      expect(cs.email, equals(TestGoogleIdToken['email']));
      expect(cs.emailVerified, equals(TestGoogleIdToken['email_verified']));
      expect(cs.hd, equals(TestGoogleIdToken['hd']));
    });

    test('encode', () {
      final cs = new GoogleOpenIdClaimSet.fromJson(TestGoogleIdToken);
      final JwsHeader header = new JwsHeader.build(
          type: JwsType.JWT, algorithm: JsonWebAlgorithm.RS256);
      final String signingInput = JoseObject.encodeSegments([header, cs]);
      expect(signingInput, equals(TestSigningInput));
    });

    test('signature', () {
      final JwaRsaSignatureContext signatureContext =
          new JwaRsaSignatureContext(null, TestPrivateKey);
      final JwsSignature signature = new JwsSignature.create(
          TestSigningInput, JsonWebAlgorithm.RS256, signatureContext);
      expect(signature.encode(), equals(TestSignature));
    });

    test('decode', () {
      final JsonWebToken jwt = decodeGoogleIdToken(TestJwt);
      final GoogleOpenIdClaimSet cs = jwt.claimSet;
      expect(cs.atHash, equals(TestGoogleIdToken['at_hash']));
      expect(cs.azp, equals(TestGoogleIdToken['azp']));
      expect(cs.cHash, equals(TestGoogleIdToken['c_hash']));
      expect(cs.email, equals(TestGoogleIdToken['email']));
      expect(cs.emailVerified, equals(TestGoogleIdToken['email_verified']));
      expect(cs.hd, equals(TestGoogleIdToken['hd']));
    });

    test('validate', () {
      final cs = new GoogleOpenIdClaimSet.fromJson(TestGoogleIdToken
        ..['exp'] = new DateTime.now().millisecondsSinceEpoch ~/ 1000);
      GoogleClaimSetValidationContext validationContext(
              String accessToken, String code) =>
          new GoogleClaimSetValidationContext(
              accessToken: accessToken, code: code);

      expect(cs.validate(validationContext(null, null)), isEmpty);

      expect(
          cs.validate(validationContext(TestGoogleAuthResponse['access_token'],
              TestGoogleAuthResponse['code'])),
          isEmpty);

      expect(
          cs.validate(validationContext('invalid-token', null)), hasLength(1));

      expect(cs.validate(validationContext('invalid-token', 'invalid-code')),
          hasLength(2));
    });
  });
}
