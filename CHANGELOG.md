#### 0.4.1
  * Enable autopublishing from CI

#### 0.4.0
  * Rename library file according to pub guidelines

#### 0.3.0
  * Upgrade dependencies and refactor package accordingly

#### 0.2.1
  * Change pub version strings to older format

#### 0.2.0
  * Use official dart-jwt as this now supports RSA keys as Google JWT claim sets requires
  * Switch to cipher package instead of depending on Google APIs Auth functions

#### 0.1.0
  * Implement access_token, code and expiry validation
