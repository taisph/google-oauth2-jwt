google-oauth2-jwt
=================

[![Build Status](https://drone.io/bitbucket.org/taisph/google-oauth2-jwt/status.png)](https://drone.io/bitbucket.org/taisph/google-oauth2-jwt/latest)

Introduction
------------

This is a parser and validator for the Google OAuth2 JWT id tokens received when
a client authenticates with Google's OAuth2 servers.

Usage
-----

To decode the contents of the `id_token` field in an authorization result

```dart
JsonWebToken<GoogleClaimSet> jwt = decodeGoogleIdToken(idToken);
```

All claims can then be accessed through `jwt.claimSet`

```dart
String email = jwt.claimSet.email;
bool emailVerified = jwt.claimSet.emailVerified;
```

Validating the claims checks for expiry, and token or code tampering

```dart
GoogleClaimSetValidationContext validationContext =
    new GoogleClaimSetValidationContext(
        accessToken: 'the-token',
        code: 'the-code');

Set<ConstraintViolation> violations = jwt.claimSet.validate(validationContext);
if (violations > 0) {
  // Id token is expired or claims may have been tampered with.
  throw new Exception(violations.join(', '));
}
```

Example
-------

This is a complete example of how it would typically be used.

```dart
var authResult = {
  'access_token': 'rYgpM7eIkUcd9e8hg9Fq2j_ai_-zvTjrKv_leEo_ra87TJgwvBWi929ThwZHp',
  'code': 'qxz9kz2IJBLLfRAdvdUxxtIepI3VTMxin9h4hP2cS6MNKA34_XrNbwl_CYGRs1',
  'id_token': {
    'iat': 1410770387,
    'exp': 1410774287,
    'iss': 'accounts.google.com',
    'sub': '12345678901234567890',
    'aud': '1234567890-youramazingapp.apps.googleusercontent.com',
    'at_hash': 'dxgKHVb2pl9g30gzBN0gog',
    'azp': '1234567890-youramazingapp.apps.googleusercontent.com',
    'c_hash': 'Yk8AmAWG2HFgSvGgTGeU-g',
    'email': 'email@example.com',
    'email_verified': false,
    'hd': 'example.com'
  }
};

JsonWebToken<GoogleClaimSet> jwt = decodeGoogleIdToken(authResult['id_token']);

GoogleClaimSetValidationContext validationContext =
    new GoogleClaimSetValidationContext(
        accessToken: authResult['access_token'],
        code: authResult['code']);

Set<ConstraintViolation> violations = jwt.claimSet.validate(validationContext);
if (violations > 0) {
  // Beware! Claims may have been tampered with.
  throw new Exception(violations.join(', '));
}

// The claims made are authentic.
```

More information
----------------

More information about OpenID Connect and OAuth 2.0 can be found on the links below:

- [OpenID Connect Core 1.0](http://openid.net/specs/openid-connect-core-1_0.html)
- [Using OAuth 2.0 for Login (OpenID Connect)](https://developers.google.com/accounts/docs/OAuth2Login)
